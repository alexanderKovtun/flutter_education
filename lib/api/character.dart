import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class CharacterApi {
  static Future getCharactersByIds(String ids) async {
    try {
      Response response =
      await http.get('https://rickandmortyapi.com/api/character/$ids');
      return json.decode(response.body);
    } catch (e) {
      return Exception(e);
    }
  }
}
