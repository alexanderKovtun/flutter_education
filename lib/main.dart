import 'package:flutter/material.dart';
import 'package:kovtun_flutter/ui/colors.dart';
import 'package:kovtun_flutter/ui/screens/characters/charactersPage.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: new ThemeData(primaryColor: AppColors.main),
      home: new HomePage(),
//      }
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CharactersPage();
  }
}
