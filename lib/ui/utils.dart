class Utils {
  static String getLinksId(List episodes) {
    String ids = "";
    for(int i=0; i<episodes.length; ++i) {
      int index = episodes[i].lastIndexOf('/');
      ids += '${episodes[i].substring(index + 1)},';
    }
    return ids;
  }
}
