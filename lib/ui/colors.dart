import 'package:flutter/material.dart';

class AppColors {
  static Color main = Color.fromRGBO(64, 81, 187, 1);
  static Color mainDark = Color.fromRGBO(50, 61, 167, 1);
  
  static Color additional1 = Color(0xffdfc54f);
  static Color additional1Dark = Color(0xfffbce00);

  static Color background = Color.fromRGBO(204, 158, 115, 1);
  
  static Color textWhite = Color.fromRGBO(255, 255, 255, 1);
  static Color textBurgundy = Color(0xff94001b);
}