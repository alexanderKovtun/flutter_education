import 'package:flutter/material.dart';
import 'package:kovtun_flutter/ui/colors.dart';
import 'package:kovtun_flutter/ui/screens/Episodes/episodesPage.dart';
import 'package:kovtun_flutter/ui/screens/Locations/locationsPage.dart';
import 'package:kovtun_flutter/ui/screens/characters/charactersPage.dart';

class Item extends StatefulWidget {
  Item(this.text, this.page, this.isActive, this.isSelectedPage,
      this.onSelectPage);

  final String text;
  final Widget page;
  final bool isActive, isSelectedPage;
  final Function onSelectPage;

  _Item createState() => _Item();
}

class _Item extends State<Item> with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _animMarginActive;
  Animation<double> _animMarginInactive;
  Animation<double> _animBorderActive;
  Animation<double> _animBorderInactive;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      duration: Duration(milliseconds: 100),
      vsync: this,
    );
    _animMarginActive =
        Tween(begin: 32.0, end: 0.0).animate(_animationController);
    _animMarginInactive =
        Tween(begin: 0.0, end: 32.0).animate(_animationController);
    _animBorderActive =
        Tween(begin: 22.0, end: 0.0).animate(_animationController);
    _animBorderInactive =
        Tween(begin: 0.0, end: 22.0).animate(_animationController);
    _animationController.addListener(() {
      setState(() {});
    });
    _animationController.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed && !widget.isActive) {
        Navigator.of(context).pop();
        Navigator.of(context).pushReplacement(new PageRouteBuilder(
            pageBuilder: (BuildContext context, _, __) {
          return widget.page;
        }, transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
          return new FadeTransition(opacity: animation, child: child);
        }));
      }
    });
  }

  @override
  void didUpdateWidget(Item oldWidget) {
    if (widget.isSelectedPage && oldWidget.isActive) {
      _animationController.forward();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          bottom: 16,
          right: widget.isActive
              ? _animMarginInactive.value
              : _animMarginActive.value,
          left: widget.isActive
              ? _animMarginActive.value
              : _animMarginInactive.value),
      child: Material(
        elevation: 10,
        shadowColor: AppColors.mainDark,
        borderRadius: BorderRadius.horizontal(
          left: widget.isActive
              ? Radius.circular(_animBorderActive.value)
              : Radius.circular(_animBorderInactive.value),
          right: widget.isActive
              ? Radius.circular(_animBorderInactive.value)
              : Radius.circular(_animBorderActive.value),
        ),
        color: AppColors.mainDark,
        child: InkWell(
          onTap: () {
            if (widget.isActive) {
              Navigator.of(context).pop();
            } else {
              widget.onSelectPage();
              _animationController.forward();
            }
          },
          child: Container(
            padding: EdgeInsets.only(right: 16, top: 10, bottom: 10),
            child: Text(
              widget.text,
              textAlign: TextAlign.right,
              style: TextStyle(
                  fontSize: 20,
                  color: AppColors.textWhite,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ),
    );
  }
}

class AppDrawer extends StatefulWidget {
  AppDrawer(this.currentRoute);

  final String currentRoute;

  _AppDrawer createState() => _AppDrawer();
}

class _AppDrawer extends State<AppDrawer> {
  bool isSelectedPage = false;

  @override
  void initState() {
    super.initState();
  }

  void _setSelectedPage() {
    setState(() {
      isSelectedPage = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: Container(
        padding: EdgeInsets.only(top: 50),
        color: AppColors.additional1,
        child: ListView(
          children: <Widget>[
            Item(
              "Characters",
              new CharactersPage(),
              widget.currentRoute == "Characters",
              isSelectedPage,
              _setSelectedPage,
            ),
            Item(
              "Locations",
              new LocationPage(),
              widget.currentRoute == "Locations",
              isSelectedPage,
              _setSelectedPage,
            ),
            Item(
              "Episodes",
              new EpisodesPage(),
              widget.currentRoute == "Episodes",
              isSelectedPage,
              _setSelectedPage,
            ),
          ],
        ),
      ),
    );
  }
}
