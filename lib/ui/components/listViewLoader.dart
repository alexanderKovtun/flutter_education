import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:kovtun_flutter/ui/colors.dart';
import 'package:kovtun_flutter/ui/components/appDrawer.dart';

class ListViewLoader extends StatefulWidget {
  ListViewLoader(
      {@required this.name, @required this.apiLink, @required this.buildItem});

  final String name, apiLink;
  final Function buildItem;

  @override
  _ListViewLoader createState() => _ListViewLoader();
}

class _ListViewLoader extends State<ListViewLoader> {
  String nextPage;

  ScrollController _scrollController = new ScrollController();

  bool isLoading = false;

  List names = new List();

  void _getMoreData() async {
    if (nextPage.length == 0) {
      return;
    }
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });

      try {
        Response response = await http.get(nextPage);
        final decoded = json.decode(response.body);
        List data = decoded["results"] as List;
        List tempList = new List();
        nextPage = decoded["info"]['next'];
        for (int i = 0; i < data.length; i++) {
          tempList.add(data[i]);
        }

        setState(() {
          isLoading = false;
          names.addAll(tempList);
        });
      } catch (e) {
        print('bla error $e');
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  @override
  void initState() {
    nextPage = widget.apiLink;
    this._getMoreData();
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _getMoreData();
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  Widget _buildProgressIndicator() {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Center(
        child: new Opacity(
          opacity: isLoading ? 1.0 : 00,
          child: new CircularProgressIndicator(),
        ),
      ),
    );
  }

  @protected
  Widget _buildList() {
    return ListView.builder(
      //+1 for progressbar
      itemCount: names.length + 1,
      itemBuilder: (BuildContext context, int index) {
        if (index == names.length) {
          return _buildProgressIndicator();
        } else {
          return widget.buildItem(names[index]);
        }
      },
      controller: _scrollController,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: AppDrawer(widget.name),
      appBar: AppBar(
        title: Text(widget.name),
      ),
      body: Container(
        child: _buildList(),
        color: AppColors.background,
      ),
      resizeToAvoidBottomPadding: false,
    );
  }
}
