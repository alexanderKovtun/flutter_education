import 'package:flutter/material.dart';

class CharacterImage extends StatelessWidget {
  final String imgUrl, tag;

  CharacterImage({@required this.imgUrl, @required this.tag});

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: imgUrl,
      child: Image.network(
        imgUrl,
      ),
    );
  }
}
