import 'package:flutter/material.dart';
import 'package:kovtun_flutter/ui/colors.dart';
import 'package:kovtun_flutter/ui/screens/characters/SingleCharacter.dart';
import 'package:kovtun_flutter/ui/screens/characters/characterImage.dart';

class CharacterItem extends StatelessWidget {
  CharacterItem({
    @required this.name,
    @required this.id,
    @required this.imgUrl,
    @required this.characterData,
    this.status = "",
    this.gender = "",
    this.species = "",
  });

  final int id;

  final Map characterData;
  final String name, status, gender, species, imgUrl;

  _navigateToSingleCharacterItem(BuildContext context, String heroImgTag) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => new SingleCharacter(
              heroImgTag: heroImgTag,
              imgUrl: imgUrl,
              name: name,
              status: status,
              gender: gender,
              species: species,
              characterData: characterData,
            ),
      ),
    );
  }

  Widget _renderInfo(String title, String value, bgColor) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8),
      color: bgColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(title,
              style: TextStyle(color: AppColors.textWhite, fontSize: 15)),
          Flexible(
            child: Text(value,
                textAlign: TextAlign.right,
                style: TextStyle(
                    color: AppColors.textWhite,
                    fontSize: 18,
                    fontWeight: FontWeight.bold)),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    String heroImgTag = 'chracter${id}img';
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 10),
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(16)),
        shadowColor: AppColors.textBurgundy,
        elevation: 8,
        color: AppColors.main,
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(16)),
          child: InkWell(
            onTap: () {
              _navigateToSingleCharacterItem(context, heroImgTag);
            },
            splashColor: Colors.amberAccent,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: 120,
                  height: 120,
                  child: CharacterImage(imgUrl: imgUrl, tag: heroImgTag),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        _renderInfo("NAME: ", name, AppColors.mainDark),
                        _renderInfo("STATUS: ", status, AppColors.main),
                        _renderInfo("GENDER: ", gender, AppColors.mainDark),
                        _renderInfo("SPECIES: ", species, AppColors.main),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
