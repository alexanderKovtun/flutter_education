import 'package:flutter/material.dart';
import 'package:kovtun_flutter/ui/screens/characters/characterItem.dart';
import 'package:kovtun_flutter/ui/components/listViewLoader.dart';

Widget _buildListItem(Map character) {
  return CharacterItem(
    name: character["name"],
    imgUrl: character["image"],
    status: character["status"],
    species: character["species"],
    gender: character["gender"],
    id: character["id"],
    characterData: character,
//    allInfo: names[index],
  );
}

class CharactersPage extends ListViewLoader {
  CharactersPage()
      : super(
            name: "Characters",
            apiLink: "https://rickandmortyapi.com/api/character/",
            buildItem: _buildListItem);
}
