import 'package:flutter/material.dart';
import 'package:kovtun_flutter/api/episode.dart';
import 'package:kovtun_flutter/ui/colors.dart';
import 'package:kovtun_flutter/ui/screens/Episodes/episodeItem.dart';
import 'package:kovtun_flutter/ui/screens/characters/characterImage.dart';
import 'package:kovtun_flutter/ui/utils.dart';

class SingleCharacter extends StatefulWidget {
  SingleCharacter({
    @required this.heroImgTag,
    @required this.imgUrl,
    @required this.name,
    @required this.characterData,
    this.status = "",
    this.gender = "",
    this.species = "",
  });

  final Map characterData;
  final String heroImgTag, imgUrl, name, status, gender, species;

  @override
  _SingleCharacter createState() => _SingleCharacter();
}

class _SingleCharacter extends State<SingleCharacter> {
  String episodesId;
  List episodes = [];

  @override
  void initState() {
    super.initState();
    episodesId = Utils.getLinksId(widget.characterData["episode"]);
    this._getCharactersById();
  }

  void _getCharactersById() async {
    List response = await EpisodesApi.getEpisodesByIds(episodesId);
    setState(() {
      episodes = response;
    });
  }

  Widget _renderInfo(String title, String value,
      {double fontSize = 18, Color bgColor}) {
    return new Container(
      color: bgColor,
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(title,
              style: TextStyle(fontSize: fontSize, color: AppColors.textWhite)),
          Flexible(
            child: Text(value,
                textAlign: TextAlign.right,
                style:
                    TextStyle(fontSize: fontSize, color: AppColors.textWhite)),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: AppColors.background,
        appBar: new AppBar(title: new Text(widget.name)),
        body: ListView(
          children: <Widget>[
            Container(
              height: 300,
              child: CharacterImage(
                imgUrl: widget.imgUrl,
                tag: widget.heroImgTag,
              ),
            ),
            _renderInfo("Name: ", widget.name, bgColor: AppColors.mainDark),
            _renderInfo("Status: ", widget.status, bgColor: AppColors.main),
            _renderInfo("Gender: ", widget.gender, bgColor: AppColors.mainDark),
            _renderInfo("Species: ", widget.species, bgColor: AppColors.main),
            _renderInfo("Origin: ", widget.characterData["origin"]["name"],
                bgColor: AppColors.mainDark),
            Container(
              padding: EdgeInsets.only(top: 20),
              child: ClipRRect(
                borderRadius: new BorderRadius.only(
                    topRight: Radius.circular(30.0),
                    topLeft: Radius.circular(30.0)),
                child: Container(
//                    height: 60,
                    padding: EdgeInsets.all(16.0),
                    alignment: Alignment.center,
                    color: AppColors.additional1,
                    child: Text(
                        "List of episodes in which this character appeared",
                        style: TextStyle(
                            fontSize: 20.0, fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center)),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: episodes
                  .map((val) => Container(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 8.0),
                        color: AppColors.additional1,
                        child: EpisodeItem(
                          name: val["name"],
                          episode: val["episode"],
                          airDate: val["air_date"],
                          episodeData: val,
                        ),
                      ))
                  .toList(),
            ),
          ],
        ));
  }
}
