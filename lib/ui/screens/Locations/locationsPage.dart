import 'package:flutter/material.dart';
import 'package:kovtun_flutter/ui/screens/Locations/locationItem.dart';
import 'package:kovtun_flutter/ui/components/listViewLoader.dart';

Widget _buildListItem(Map location) {
  return LocationItem(
    name: location["name"],
    type: location["type"],
    dimension: location["dimension"],
    locationData: location,
  );
}

class LocationPage extends ListViewLoader {
  LocationPage()
      : super(
            name: "Locations",
            apiLink: "https://rickandmortyapi.com/api/location/",
            buildItem: _buildListItem);
}
