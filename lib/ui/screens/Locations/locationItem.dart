import 'package:flutter/material.dart';
import 'package:kovtun_flutter/ui/colors.dart';
import 'package:kovtun_flutter/ui/screens/Locations/singleLocationPage.dart';
import 'package:kovtun_flutter/ui/utils.dart';

class LocationItem extends StatefulWidget {
  LocationItem({
    @required this.name,
    @required this.locationData,
    @required this.type,
    this.dimension = "",
  });

  final Map locationData;
  final String name, dimension, type;

  @override
  _LocationItem createState() => _LocationItem();
}

class _LocationItem extends State<LocationItem> with SingleTickerProviderStateMixin {
  bool isTap = false;
  AnimationController _animationController;
  Animation<double> _animationTween;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      duration: Duration(milliseconds: 30),
      vsync: this,
    );
    _animationTween =
        Tween(begin: 9.0, end: 0.0).animate(_animationController);
    _animationController.addListener(() {
      setState(() {});
    });
  }

  _navigateToSingleLocationItem(BuildContext context) {
    String charactersId = Utils.getLinksId(widget.locationData["residents"]);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => new SingleLocationPage(
              name: widget.name,
              locationData: widget.locationData,
              charactersId: charactersId,
            ),
      ),
    );
  }

  Widget _renderTextRow(String rowName, String value, Color bgColor) {
    return Container(
      color: bgColor,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(rowName,
              style: TextStyle(color: AppColors.textWhite, fontSize: 18)),
          Flexible(
            child: Text(value,
                textAlign: TextAlign.right,
                style: TextStyle(color: AppColors.textWhite, fontSize: 18)),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      child: Material(
        elevation: _animationTween.value,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        shadowColor: AppColors.textBurgundy,
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: GestureDetector(
            onTapDown: (_) {
              _animationController.forward();
              setState(() {
                isTap = true;
              });
              Future.delayed(const Duration(milliseconds: 100), () {
                setState(() {
                  isTap = false;
                });
                _animationController.reverse();
              });
            },
            onTap: () {
              _navigateToSingleLocationItem(context);
            },
            child: AnimatedOpacity(
              duration: isTap
                  ? Duration(milliseconds: 50)
                  : Duration(milliseconds: 800),
              opacity: isTap ? 0.3 : 1,
              child: Container(
                color: AppColors.main,
                child: Column(
                  children: <Widget>[
                    _renderTextRow("Name: ", widget.name, AppColors.mainDark),
                    _renderTextRow("Type: ", widget.type, AppColors.main),
                    _renderTextRow(
                        "Dimension: ", widget.dimension, AppColors.mainDark),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
