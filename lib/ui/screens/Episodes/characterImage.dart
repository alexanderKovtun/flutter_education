import 'package:flutter/material.dart';
import 'package:kovtun_flutter/ui/colors.dart';
import 'package:kovtun_flutter/ui/screens/characters/SingleCharacter.dart';
import 'package:kovtun_flutter/ui/screens/characters/characterImage.dart';

class CharacterImageTile extends StatelessWidget {
  CharacterImageTile(this.character);

  final Map character;

  _navigateToSingleCharacterItem(BuildContext context, String heroImgTag) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => new SingleCharacter(
              heroImgTag: heroImgTag,
              imgUrl: character["image"],
              name: character["name"],
              status: character["status"],
              gender: character["gender"],
              species: character["species"],
              characterData: character,
            ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    String heroImgTag = 'episodechracter${character["id"]}img';
    return InkWell(
      onTap: () {
        _navigateToSingleCharacterItem(context, heroImgTag);
      },
      child: Material(
        elevation: 6,
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        shadowColor: AppColors.mainDark,
        child: Container(
          width: MediaQuery.of(context).size.width * 0.30,
          height: MediaQuery.of(context).size.width * 0.30,
          constraints: BoxConstraints(
              minWidth: 100, minHeight: 100, maxWidth: 250, maxHeight: 250),
          decoration: BoxDecoration(
            border: Border.all(
              width: 3.0,
              color: AppColors.additional1Dark,
            ),
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
          ),
          child: Stack(
            children: <Widget>[
              Hero(
                tag: heroImgTag,
                child:
                CharacterImage(imgUrl: character["image"], tag: heroImgTag),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
