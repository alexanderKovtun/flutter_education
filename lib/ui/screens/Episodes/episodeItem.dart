import 'package:flutter/material.dart';
import 'package:kovtun_flutter/ui/colors.dart';
import 'package:kovtun_flutter/ui/screens/Episodes/singleEpisodePage.dart';
import 'package:kovtun_flutter/ui/utils.dart';

class EpisodeItem extends StatefulWidget {
  EpisodeItem(
      {@required this.name,
      @required this.airDate,
      @required this.episodeData,
      this.episode = ""});

  final Map episodeData;
  final String name, episode, airDate;

  @override
  _EpisodeItem createState() => _EpisodeItem();
}

class _EpisodeItem extends State<EpisodeItem>
    with SingleTickerProviderStateMixin {
  bool isTap = false;
  AnimationController _animationController;
  Animation<double> _animationTween;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      duration: Duration(milliseconds: 30),
      vsync: this,
    );
    _animationTween =
        Tween(begin: 9.0, end: 0.0).animate(_animationController);
    _animationController.addListener(() {
      setState(() {});
    });
  }

  _navigateToSingleCharacterItem(BuildContext context) {
    String charactersId = Utils.getLinksId(widget.episodeData["characters"]);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => new SingleEpisodePage(
            name: widget.name,
            episodeData: widget.episodeData,
            charactersId: charactersId,
            heroTag: "episode${widget.episodeData["id"]}"),
      ),
    );
  }

  Widget _renderTextRow(String rowName, String value, Color bgColor) {
    return Container(
      color: bgColor,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(rowName,
              style: TextStyle(color: AppColors.textWhite, fontSize: 18)),
          Flexible(
            child: Text(value,
                textAlign: TextAlign.right,
                style: TextStyle(color: AppColors.textWhite, fontSize: 18)),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        elevation: _animationTween.value,
        shadowColor: AppColors.textBurgundy,
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: GestureDetector(
            onTapDown: (_) {
              setState(() {
                isTap = true;
              });
              _animationController.forward();
              Future.delayed(const Duration(milliseconds: 100), () {
                setState(() {
                  isTap = false;
                });
                _animationController.reverse();
              });
            },
            onTap: () {
              _navigateToSingleCharacterItem(context);
            },
            child: AnimatedOpacity(
              duration: isTap
                  ? Duration(milliseconds: 50)
                  : Duration(milliseconds: 800),
              opacity: isTap ? 0.3 : 1,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                color: AppColors.main,
                child: Column(
                  children: <Widget>[
                    _renderTextRow("Name: ", widget.name, AppColors.mainDark),
                    _renderTextRow("EPISODE: ", widget.episode, AppColors.main),
                    _renderTextRow(
                        "AIR DATE: ", widget.airDate, AppColors.mainDark),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
