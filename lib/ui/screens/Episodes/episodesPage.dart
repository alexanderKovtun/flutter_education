import 'package:flutter/material.dart';
import 'package:kovtun_flutter/ui/screens/Episodes/episodeItem.dart';
import 'package:kovtun_flutter/ui/components/listViewLoader.dart';

Widget _buildListItem(Map episode) {
  return EpisodeItem(
    name: episode["name"],
    episode: episode["episode"],
    airDate: episode["air_date"],
    episodeData: episode,
  );
}

class EpisodesPage extends ListViewLoader {
  EpisodesPage()
      : super(
            name: "Episodes",
            apiLink: "https://rickandmortyapi.com/api/episode/",
            buildItem: _buildListItem);
}
