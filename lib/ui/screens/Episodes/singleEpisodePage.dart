import 'package:flutter/material.dart';
import 'package:kovtun_flutter/api/character.dart';
import 'package:kovtun_flutter/ui/colors.dart';
import 'package:kovtun_flutter/ui/screens/Episodes/characterImage.dart';

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border.all(width: 3.0),
    borderRadius: BorderRadius.all(Radius.circular(5.0)),
  );
}

class SingleEpisodePage extends StatefulWidget {
  SingleEpisodePage(
      {@required this.name,
      @required this.episodeData,
      @required this.heroTag,
      this.charactersId});

  final Map episodeData;
  final String name, charactersId, heroTag;

  @override
  _SingleEpisodePage createState() => _SingleEpisodePage();
}

class _SingleEpisodePage extends State<SingleEpisodePage> {
  List characters = [];

  @override
  void initState() {
    super.initState();
    this._getCharactersById();
  }

  void _getCharactersById() async {
    List response = await CharacterApi.getCharactersByIds(widget.charactersId);
    setState(() {
      characters = response;
    });
  }

  Widget _renderInfo(String title, String value,
      {double fontSize = 18, Color bgColor}) {
    return new Material(
      child: Container(
        color: bgColor,
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(title,
                style:
                    TextStyle(fontSize: fontSize, color: AppColors.textWhite)),
            Flexible(
              child: Text(value,
                  textAlign: TextAlign.right,
                  style: TextStyle(
                      fontSize: fontSize, color: AppColors.textWhite)),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text(widget.name)),
      body: Container(
        color: AppColors.background,
        child: ListView(
          children: <Widget>[
            Container(
              padding: new EdgeInsets.only(top: 10, bottom: 10),
              child: Column(
                children: <Widget>[
                  _renderInfo("NAME: ", widget.name,
                      bgColor: AppColors.mainDark),
                  _renderInfo("EPISODE: ", widget.episodeData["episode"],
                      bgColor: AppColors.main),
                  _renderInfo("AIR DATE: ", widget.episodeData["air_date"],
                      bgColor: AppColors.mainDark),
                ],
              ),
            ),
            ClipRRect(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30), topRight: Radius.circular(30)),
              child: Container(
//                margin: EdgeInsets.only(bottom: 15),
                padding: EdgeInsets.all(10),
                color: AppColors.additional1,
                child: Text(
                  "List of characters who have been seen in the episode.",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
            Container(
              color: AppColors.additional1,
              child: Wrap(
                alignment: WrapAlignment.spaceAround,
                spacing: 8.0, // gap between adjacent chips
                runSpacing: 8.0, // gap between lines
                children: characters.map((val) {
                  return CharacterImageTile(val);
                }).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
